'use strict';
describe('orderByLanguageName', function() {
  beforeEach(module('homerez.languages'));

  describe('orderByLanguageName in English', function() {
    beforeEach(inject(function(_$translate_) {
      sinon.stub(_$translate_, 'use', function() {return 'en'});
    }));

    it('should return as many results as hipLanguages.getCodes does',
      inject(function(orderByLanguageNameFilter) {
        var result = orderByLanguageNameFilter(hipLanguages.getCodes());
        expect(result).to.have.length(hipLanguages.getCodes().length);
    }));

    it('should show "Dutch" before "German"',
      inject(function(orderByLanguageNameFilter) {
        var result = orderByLanguageNameFilter(hipLanguages.getCodes());
        expect(result.indexOf('nl')).to.be.below(result.indexOf('de'))
    }));

  })

})
describe('languageName', function() {

  beforeEach(module('homerez.languages'));

  var tests = [
    {
      blockName: 'languageNames in English',
      language: 'en',
      testCases: [
        {
          languageCode: 'nl', 
          expectedLanguageName: 'Dutch'
        },
        {
          languageCode: 'en', 
          expectedLanguageName: 'English'
        }
      ]
    },
    {
      blockName: 'languageNames in Spanish', 
      language: 'es',
      testCases: [
        {
          languageCode: 'de',
          expectedLanguageName: 'alemán'
        },
        {
          languageCode: 'fr',
          expectedLanguageName: 'francés'
        }
      ]
    },
    {
      blockName: 'languageNames in French', 
      language: 'fr',
      testCases: [
        {
          languageCode: 'it',
          expectedLanguageName: 'italien'
        },
        {
          languageCode: 'da',
          expectedLanguageName: 'danois'
        }
      ]
    },
    {
      blockName: 'languageNames in German', 
      language: 'de',
      testCases: [
        {
          languageCode: 'pt',
          expectedLanguageName: 'Portugiesisch'
        },
        {
          languageCode: 'sv',
          expectedLanguageName: 'Schwedisch'
        }
      ]
    }
  ];
  for (var i = 0; i < tests.length; i++) {
    var t = tests[i];
    describe(t.blockName, function() {

      var lang = t.language;
      beforeEach(inject(function(_$translate_) {
        sinon.stub(_$translate_, 'use', function() {return lang});
      }));

      for (var j = 0; j<t.testCases.length; j++) {
        var tc = t.testCases[j];
        var testName = 'should convert "'+tc.languageCode+'" into "'+tc.expectedLanguageName+'"';

        it(testName, inject(function(languageNameFilter) {
          expect(languageNameFilter(tc.languageCode)).to.equal(tc.expectedLanguageName);
        }));
      }
    });
  }  
});
