'use strict';

var languageCodes = ['da', 'de', 'en', 'es', 'fi', 'fr', 'it', 'nl', 'no', 'pt', 'sv'];
var languageNames = ['Danish', 'Dutch', 'English', 'Finnish', 'French', 'German', 'Italian', 'Norwegian', 'Portugese', 'Spanish', 'Swedish'];

describe("getCodes", function () {
    it("should return the HIP supported languages", function () {
        hipLanguages.getCodes().should.deep.equal(languageCodes);
    });
});
describe("getNames", function () {
    it("should return the HIP supported language names in English", function () {
        hipLanguages.getNames('en').should.deep.equal(languageNames);
    });
    it("should return the HIP supported language names in English without supplying a parameter", function () {
        hipLanguages.getNames().should.deep.equal(languageNames);
    });
    it("should return the HIP supported language names in English when the language is not supported", function () {
        hipLanguages.getNames('xx').should.deep.equal(languageNames);
    });
});
describe("getNameByCode", function () {
    it("should return 'English' for 'en'", function () {
        hipLanguages.getNameByCode('en').should.equal('English');
    });
    it("should return 'Dutch' for 'nl'", function () {
        hipLanguages.getNameByCode('nl').should.equal('Dutch');
    });
    it("should return 'Swedish' for 'sv'", function () {
        hipLanguages.getNameByCode('sv').should.equal('Swedish');
    });
    it("should return 'unknown' for 'xx'", function () {
        hipLanguages.getNameByCode('xx').should.equal('unknown');
    });
    it("should return 'unknown' when no code is specified", function () {
        hipLanguages.getNameByCode().should.equal('unknown');
    });
});
describe("getNameByCode with other languages than default", function () {
    it("should return 'inglés' for 'en' in Spanish", function () {
        hipLanguages.getNameByCode('en', 'es').should.equal('inglés');
    });
    it("should return 'néerlandais' for 'nl' in French", function () {
        hipLanguages.getNameByCode('nl', 'fr').should.equal('néerlandais');
    });
    it("should return 'Schwedisch' for 'sv' in German", function () {
        hipLanguages.getNameByCode('sv', 'de').should.equal('Schwedisch');
    });
    it("should return 'unknown' for 'xx' in 'de'", function () {
        hipLanguages.getNameByCode('xx', 'de').should.equal('unknown');
    });
    it("should return 'unknown' in 'de' when no code is specified", function () {
        hipLanguages.getNameByCode(null, 'de').should.equal('unknown');
    });
});
