# hipLanguages 0.3.0

Utility functions for languages supported in HIP

## getNames()
Get all English language names

## getCodes()
get all two-letter language codes

## getNameByCode(code)
Get an English language name by its two-letter code
