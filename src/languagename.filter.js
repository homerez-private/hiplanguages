(function() {
'use strict';

angular.module('homerez.languages', ['pascalprecht.translate'])
  .filter('languageName', languageName)
  .filter('orderByLanguageName', orderByLanguageName);

  orderByLanguageName.$inject = ['$translate', '$filter'];
  languageName.$inject        = ['$translate'];


  function orderByLanguageName($translate, $filter) {
	  return function(array, objKey) {
	    var result = [];
	    var translated = [];
	    angular.forEach(array, function(value) {
	      var translateKey = objKey ? value[objKey] : value;
        var currentLanguage = $translate.use() || 'en';
	      translated.push({
	        key: value,
	        label: hipLanguages.getNameByCode(translateKey, currentLanguage.substr(0,2))
	      });
	    });
	    angular.forEach($filter('orderBy')(translated, 'label'), function(sortedObject) {
	      result.push(sortedObject.key);
	    });
	    return result;
	  };
	}

  function languageName($translate) {
    return function(languageCode) {
      var currentLanguage = $translate.use() || 'en';
      return hipLanguages.getNameByCode(languageCode, currentLanguage.substr(0,2));
    };
  }

}());
