(function (root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    // Browser globals (root is window)
    root.hipLanguages = factory();
  }
}(this, function () {

  var supportedLanguages = ['en', 'es', 'fr', 'de', 'it'];

  var languageData = {
    en: {
        en: "English",
        fr: "anglais",
        es: "inglés",
        de: "Englisch",
        it: "inglese"
    },
    fr: {
        en: "French",
        fr: "français",
        es: "francés",
        de: "Französisch",
        it: "francese"
    },
    de: {
        en: "German",
        fr: "allemand",
        es: "alemán",
        de: "Deutsch",
        it: "tedesco"
    },
    nl: {
        en: "Dutch",
        fr: "néerlandais",
        es: "neerlandés",
        de: "Niederländisch",
        it: "olandese"
    },
    it: {
        en: "Italian",
        fr: "italien",
        es: "italiano",
        de: "Italienisch",
        it: "italiano"
    },
    es: {
        en: "Spanish",
        fr: "espagnol",
        es: "español",
        de: "Spanisch",
        it: "spagnolo"
    },
    pt: {
        en: "Portuguese",
        fr: "portugais",
        es: "portugués",
        de: "Portugiesisch",
        it: "portoghese"
    },
    da: {
        en: "Danish",
        fr: "danois",
        es: "danés",
        de: "Dänisch",
        it: "danese"
    },
    no: {
        en: "Norwegian",
        fr: "norvégien",
        es: "noruego",
        de: "Norwegisch",
        it: "norvegese"
    },
    fi: {
        en: "Finnish",
        fr: "finnois",
        es: "finés",
        de: "Finnisch",
        it: "finlandese"
    },
    sv: {
        en: "Swedish",
        fr: "suédois",
        es: "sueco",
        de: "Schwedisch",
        it: "svedese"
    },
    hr: {
        en: "Croatian",
        fr: "Croate",
        es: "Croata",
        de: "Kroatisch",
        it: "Croato"
    },
    gr: {
        en: "Greek",
        fr: "Grec",
        es: "Griego",
        de: "Griechisch",
        it: "Greco"
    },
    sr: {
        en: 'Serbian',
        fr: 'Serbe',
        es: 'Serbio',
        de: 'Serbish',
        it: 'Serbo'
    },
    id: {
        en:'Bahasa Indonesia'
    },
    ms: {
        en: 'Bahasa Malay'
    },
    ca: {
        en: 'Catalan'
    },
    cs: {
        en: 'Czech'
    },
    et: {
        en: 'Estonian'
    },
    tl: {
        en: 'Filipino'
    },
    is: {
        en: 'Icelandic'
    },
    lv: {
        en: 'Latvian'
    },
    lt: {
        en: 'Lithuanian'
    },
    hu: {
        en: 'Hungarian'
    },
    pl: {
        en: 'Polish'
    },
    ru: {
        en: 'Russian'
    },
    ro: {
        en:'Romanian'
    },
    sk: {
        en: 'Slovak'
    },
    sl: {
        en: 'Slovene'
    },
    tr: {
        en: 'Turkish'
    },
    vi: {
        en: 'Viêt Namese'
    },
    bg: {
        en: 'Bulgarian'
    },
    uk:{
        en: 'Ukrainian'
    },
    th: {
        en: 'Thai'
    },
    he: {
        en: 'Hebrew'
    },
    ar: {
        en: 'Arabic'
    },
    ko: {
        en: 'Korean'
    },
    ja: {
        en: 'Japanese'
    },
    zh: {
        en: 'Chinese'
    }

  };

  function getNames(language) {
    var names = [];
    var language = language || 'en';
    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }

    Object.keys(languageData).forEach(function(language_code, index) {
      if (languageData[language_code][language]) {
        names.push(languageData[language_code][language]);
      }
    });

    names = names.sort(function(a,b) {
      return a.localeCompare(b);
    });

    return names;
  };

  function getCodes() {
    var result = Object.keys(languageData).sort();
    return result;
  };

  function getNameByCode(code, language) {
    language = language || 'en';
    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }
    var result = 'unknown';
    if (code) {
      if (Object.keys(languageData).indexOf(code) !== -1) {
        result = languageData[code][language];
      }
    }
    return result;
  };

  return {
    getNames     : getNames,
    getCodes     : getCodes,
    getNameByCode: getNameByCode
  };
}));
